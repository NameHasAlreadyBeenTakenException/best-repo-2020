# Payme

## 1. Aplikacja

### 1.1. O aplikacji

Payme jest aplikacją, która ma udostępnić prostą metodę płatności za pośrednictwem zeskanowanych kodów QR. Odbiorca generuje w aplikacji kod QR, a nadawca po zeskanowaniu ma możliwość wykonania płatności w prosty sposób. Aktualne dostępne są dwie metody płatności: PayPal i BLIK.

### 1.2. PayPal

Odbiorca posiadający konto na PayPalu może wygenerować kod QR, zawierający informacje niezbędne do wykonania płatności. Nadawca może to zrobić za pośrednictwem PayPala lub dokonać płatności kartą, jeśli nie posiada konta PayPal.

### 1.3. BLIK

Drugą opcją jest jest wykonanie transferu przez BLIKa. Wymagana jest aktywacja mobilnych transferów po stornie odbiorcy. Aplikacja po zeskanowaniu kodu QR otwiera u nadawcy aplikację bankową i kopiuje przekazany w kodzie QR nr telefonu do schowka (jest niezbędny do wykonania transferu). Następnie korzystając ze swojej aplikacji bankowej można w prosty sposób wykonać transfer, wklejając nr telefonu i podając kwotę.

### 1.4. A co jak nie mam aplikacji?

Nadawca może dokonać płatności nie posiadając aplikacji - istnieje możliwość wygenerowania innej wersji QR kodu, która przekierowuje bezpośrednio do strony internetoway PayPal i umożliwia dokonanie wpłaty. Tutaj również płatności można dokonać kartą.

## 2. Możliwości wykorzystania

### 2.1.  Bezgotówkowe napiwki

Współcześnie coraz częściej płacimy w restauracjach korzystając z płatności bezgotówkowych. Żeby zostawić napiwek często pojawia się konieczność skorzystania z gotówki, co może być niewygodne, w sytuacji kiedy staramy się tej gotówki unikać. Aplikacja umożliwi dokonanie szybkiego przelewu napiwka poprzez zeskanowanie kodu QR wygenerowanego przez kelnera.

### 2.2. Szybkie płatności do znajomych

W grupie znajomych mamy możliwość szybkiego rozliczenia się, np. przy wspólnym wyjściu do restauracji. Jedna osoba płaci za wszystkich, a następnie udostępnia swój QR kod znajomym, którzy mogą szybko oddać pieniądze.

### 2.3. Uliczni sprzedawcy

Osoby sprzedające np. owoce na ulicach często nie dysponują terminalem umożliwiającym płatnośc kartą. Dzięki aplikacji, klienci mogliby szybko zapłacić po zeskanowaniu kodu QR.

### 2.4. Uliczni performerzy

Podobnie w przypadku ulicznych artystów aplikacja ułatwiłaby bezgotówkowe przelewy. Artysta udostępniając swój QR kod widowni może otrzymywać wpłaty.

## 3. Potencjał do dalszego rozwoju

### 3.1 Nowe metody płatności

Można rozszerzyć funkcjonalność aplikacji o nowe metody płatności (GooglePay, ApplePay, itp.), co zwiększyłoby grono potencjalnych użytkowników. Dodatkowo można dołączyć funkcjonalność podłączenia karty płatniczej pod aplikację.

### 3.2. Współpraca z restauracjami

Aby ułatwić dawanie napiwków, restauracje mogłyby generować na paragonie QR kod kelnera obsługującego grupę osób, co jeszcze bardziej ułatwiłoby proces płatności.

## 4. Screeny z aplikacji

<img src="doc/main.jpg" width="250px" alt="">
<img src="doc/methods1.jpg" width="250px" alt="">
<img src="doc/methods2.jpg" width="250px" alt="">

<img src="doc/qr1.jpg" width="250px" alt="">
<img src="doc/qr2.jpg" width="250px" alt="">
<img src="doc/qr3.jpg" width="250px" alt="">

<img src="doc/pay1.jpg" width="250px" alt="">
<img src="doc/pay2.jpg" width="250px" alt="">
<img src="doc/pay3.jpg" width="250px" alt="">

