package com.nnfe.bestapp2020

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.nnfe.bestapp2020.model.Payment
import com.nnfe.bestapp2020.paymentsRV.PaymentsAdapter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ManagePaymentsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ManagePaymentsFragment : Fragment() {

    lateinit var payments: ArrayList<Payment>
    lateinit var rvPayments: RecyclerView
    lateinit var adapter: PaymentsAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_payments, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            AddPaymentsDialog(view.context, this).show()
        }

        rvPayments = view.findViewById<View>(R.id.payment_methods) as RecyclerView

        payments = Payment.getPaymentMethodsData(view.context.getSharedPreferences(Payment.PaymentPrefsKey, Context.MODE_PRIVATE))

        adapter = PaymentsAdapter(payments, this)

        rvPayments.adapter = adapter

        rvPayments.layoutManager = LinearLayoutManager(view.context)
    }

    fun OpenPayPalFragment(content: String)
    {
        val bundle = bundleOf(Pair("content", content))
        findNavController().navigate(R.id.action_Manage_to_PayPal, bundle)
    }

    fun OpenBlikFragment(content: String)
    {
        val bundle = bundleOf(Pair("content", content))
        findNavController().navigate(R.id.action_Manage_to_Blik, bundle)
    }

    fun ItemRemoved(position: Int)
    {
        payments.removeAt(position)
        rvPayments.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, payments.size);
    }

}