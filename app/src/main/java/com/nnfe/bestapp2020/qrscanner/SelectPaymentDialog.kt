package com.nnfe.bestapp2020

import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.Toast
import androidx.core.content.getSystemService


class SelectPaymentDialog(
    private val dialog_context: Context, private val payee_email: String? = null,
    private val payee_phone_number: String? = null) : Dialog(dialog_context)
{

    init {
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.payments_dialog)
        val payPalButton = findViewById<Button>(R.id.PayPal_button)
        val blikButton = findViewById<Button>(R.id.Blik_button)

        if (payee_phone_number.isNullOrEmpty())
            blikButton.visibility = Button.GONE

        if (payee_email.isNullOrEmpty())
            payPalButton.visibility = Button.GONE

        findViewById<Button>(R.id.cancel_button).visibility = Button.GONE

        payPalButton.setOnClickListener {
            val intent = Intent(dialog_context, PaymentDetailsActivity::class.java)
            intent.putExtra(PaymentDetailsActivity.PAYEE_EMAIL, payee_email)
            this.dismiss()
            dialog_context.startActivity(intent)
        }

        blikButton.setOnClickListener {
            val clipboard = dialog_context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Phone number", payee_phone_number!!)
            clipboard.setPrimaryClip(clip)

            val mainIntent = Intent(dialog_context, MainActivity::class.java)
            if (!tryOpenAllPossibleBanks()) {
                Toast.makeText(
                    dialog_context,
                    "Nie znaleziono aplikacji banku, numer telefonu został skopiowany.",
                    Toast.LENGTH_SHORT
                ).show()
                dialog_context.startActivity(mainIntent)
            }
        }

    }

    private fun tryOpenAllPossibleBanks(): Boolean {
        val mainIntent = Intent(dialog_context, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        for (pack in banksPackages) {
            val launchIntent: Intent? = dialog_context.packageManager.getLaunchIntentForPackage(pack)
            if (launchIntent != null) {
                this.dismiss()
                dialog_context.startActivity(mainIntent)
                dialog_context.startActivity(launchIntent)
                Toast.makeText(dialog_context, "Skopiowano numer telefonu", Toast.LENGTH_SHORT).show()
                return true
            }
        }

        return false
    }

    companion object {
        val banksPackages = arrayOf(
            "pl.pkobp.iko",
            "softax.pekao.powerpay",
            "pl.mbank",
            "pl.ing.mojeing",
            "pl.aliorbank.aib",
            "com.finanteq.finance.bgz",
            "com.finanteq.finance.ca",
            "wit.android.bcpBankingApp.millenniumPL",
            "pl.santanderconsumer",
            "com.asseco.hybrid.bos.prod",
            "pl.bzwbk.bzwbk24"
        )
    }
}