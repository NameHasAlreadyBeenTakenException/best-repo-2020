package com.nnfe.bestapp2020.qrscanner

import android.app.Activity
import android.content.Intent
import android.media.Image
import android.util.Log
import android.util.Size
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.nnfe.bestapp2020.PaymentDetailsActivity
import com.nnfe.bestapp2020.QRScannerActivity
import com.nnfe.bestapp2020.SelectPaymentDialog
import com.nnfe.bestapp2020.qrgenerator.QrData
import java.lang.Exception
import java.util.concurrent.atomic.AtomicBoolean


class QRScanner {
    companion object {
        var isProcessing : AtomicBoolean = AtomicBoolean(false)

        fun processFrame(imageData: Image, size: Size, activity: Activity) {
            val options = BarcodeScannerOptions.Builder()
                .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                .build()
            val scanner = BarcodeScanning.getClient(options)
            val image = InputImage.fromMediaImage(imageData, 0)
            val result = scanner.process(image)
                .addOnSuccessListener { barcodes ->
                    for (barcode in barcodes) {
                        val rawValue = barcode.rawValue
                        Log.d("QR_CODE", "processFrame: $rawValue")
                        try {
                            val qrData = QrData().parseQrData(rawValue!!)
                            (activity as QRScannerActivity).closeCamera()
                            SelectPaymentDialog(activity, qrData.paypalEmail, qrData.blikPhone).show()
                            break
                        }
                        catch (e: Exception) {

                        }
                    }
                }
                .addOnFailureListener {
                    Log.e("QR_CODE", "processFrame: ${it.message}")
                }
                .addOnCompleteListener {
                    imageData.close()
                    isProcessing.set(false)
                }
        }
    }
}