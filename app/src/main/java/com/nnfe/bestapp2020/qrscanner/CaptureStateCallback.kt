package com.nnfe.bestapp2020.qrscanner

import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraDevice
import android.view.Surface

class CaptureStateCallback
    (private val camera : CameraDevice, private val surfaces: MutableList<Surface>) : CameraCaptureSession.StateCallback()
{
    override fun onConfigureFailed(session: CameraCaptureSession) {

    }

    override fun onConfigured(session: CameraCaptureSession) {
        val builder = camera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
        builder.addTarget(surfaces[0])
        builder.addTarget(surfaces[1])
        val request = builder.build()
        session.setRepeatingRequest(request,
            CaptureCallback(surfaces[0]), null)
    }

    override fun onClosed(session: CameraCaptureSession) {
        super.onClosed(session)
        session.close()
    }
}
