package com.nnfe.bestapp2020.qrscanner

import android.hardware.camera2.CameraCharacteristics
import android.util.Log
import android.view.SurfaceHolder

class SurfaceCallback(
    private val surfaceView: ScalableSurfaceView,
    private val characteristics: CameraCharacteristics,
    private val onCreated: () -> Unit
) : SurfaceHolder.Callback {

    override fun surfaceDestroyed(holder: SurfaceHolder) = Unit

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) = Unit

    override fun surfaceCreated(holder: SurfaceHolder) {
        val previewSize = getPreviewOutputSize(surfaceView.display, characteristics, SurfaceHolder::class.java)
        Log.d(TAG, "View finder size: ${surfaceView.width} x ${surfaceView.height}")
        Log.d(TAG, "Selected preview size: $previewSize")
        surfaceView.holder.setFixedSize(previewSize.width, previewSize.height)
        surfaceView.setAspectRatio(previewSize)
        onCreated()
    }

    companion object {
        private val TAG = SurfaceCallback::class.java.simpleName
    }
}