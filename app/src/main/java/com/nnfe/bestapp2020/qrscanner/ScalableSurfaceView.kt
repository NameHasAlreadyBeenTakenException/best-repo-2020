package com.nnfe.bestapp2020.qrscanner

import android.content.Context
import android.util.AttributeSet
import android.util.Size
import android.view.SurfaceView
import kotlin.math.roundToInt

class ScalableSurfaceView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
    ) : SurfaceView(context, attrs, defStyle){

    private var aspectRatio = 0f

    private var realWidth = 0
    private var  realHeight = 0

    fun setAspectRatio(originalSize: Size) {
        val width = originalSize.width
        val height = originalSize.height
        aspectRatio = width.toFloat() / height.toFloat()
        requestLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)
        realWidth = width
        realHeight = height
        if (aspectRatio == 0f) {
            setMeasuredDimension(width, height)
        } else {

            val newWidth: Int
            val newHeight: Int
            val actualRatio = if (width > height) aspectRatio else 1f / aspectRatio
            if (width < height * actualRatio) {
                newHeight = height
                newWidth = (height * actualRatio).roundToInt()
            } else {
                newWidth = width
                newHeight = (width / actualRatio).roundToInt()
            }

            setMeasuredDimension(newWidth, newHeight)
        }
    }

}