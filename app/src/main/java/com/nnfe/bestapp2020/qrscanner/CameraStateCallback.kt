package com.nnfe.bestapp2020.qrscanner

import android.app.Activity
import android.app.Service
import android.graphics.ImageFormat
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.params.StreamConfigurationMap
import android.media.ImageReader
import android.util.Size
import android.view.Surface
import android.widget.RelativeLayout
import java.security.PrivateKey

class CameraStateCallback(
    private val configurationMap: StreamConfigurationMap,
    private val previewSurface: Surface,
    private val activity : Activity
) : CameraDevice.StateCallback() {
    override fun onDisconnected(camera: CameraDevice) {
        //camera.close()
    }

    override fun onError(camera: CameraDevice, error: Int) {
        val i = 0;
    }

    override fun onClosed(camera: CameraDevice) {
        super.onClosed(camera)
        //camera.close()
    }
    var imageReader: ImageReader? = null
    var camera : CameraDevice? = null
    override fun onOpened(camera: CameraDevice) {
        this.camera = camera
        val format = ImageFormat.YUV_420_888
        val sizes = configurationMap.getOutputSizes(format)

        var size = sizes.last()
        for (s in sizes){
            if(s.width < 2000)
            {
                size = s
                break
            }
        }

//        val containerParams = PreviewContainer.Layout?.layoutParams
//        val srcSize = PreviewContainer.Size
//        val aspect = size?.height?.div(size.width.toDouble())
//        val dstHeight = srcSize?.width?.div(aspect!!)
//
//        containerParams?.height = srcSize!!.height/2//1920/2//dstHeight!!.toInt()
//        containerParams?.width = srcSize!!.width/2//1080/2//srcSize!!.width
        //PreviewContainer.Layout?.layoutParams = containerParams
        PreviewContainer.Surface!!.setAspectRatio(size)

        imageReader = ImageReader.newInstance(size.width, size.height, format, 50)
        imageReader!!.setOnImageAvailableListener(
            fun(r: ImageReader) {
                if(QRScanner.isProcessing.get())
                {
                    return
                }
                QRScanner.isProcessing.set(true)

                val image = r.acquireLatestImage() ?: return

                QRScanner.processFrame(image, size, activity)
            }, null
        )
        val surfaces = mutableListOf(imageReader!!.surface, previewSurface)
        camera.createCaptureSession(surfaces, CaptureStateCallback(camera, surfaces), null)
    }
}