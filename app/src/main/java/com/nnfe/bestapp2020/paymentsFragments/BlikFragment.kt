package com.nnfe.bestapp2020.paymentsFragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.nnfe.bestapp2020.R
import com.nnfe.bestapp2020.model.Payment
import kotlinx.android.synthetic.main.fragment_blik.*
import kotlinx.android.synthetic.main.fragment_pay_pal.*

class BlikFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blik, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val content = arguments?.getString("content")
        blik_number.setText(content)
        save_blik_button.setOnClickListener {
            val sharedPrefs: SharedPreferences = view.context.getSharedPreferences(Payment.PaymentPrefsKey, Context.MODE_PRIVATE)
            sharedPrefs.edit().putString(Payment.BlikNumberKey, blik_number.text.toString()).commit()
            findNavController().popBackStack()
        }
    }
}