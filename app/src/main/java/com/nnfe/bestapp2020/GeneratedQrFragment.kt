package com.nnfe.bestapp2020

import android.content.Context
import android.os.Bundle
import android.text.Layout
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AlignmentSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.nnfe.bestapp2020.model.Payment
import com.nnfe.bestapp2020.qrgenerator.QrData
import com.nnfe.bestapp2020.qrgenerator.QrGenerator
import kotlinx.android.synthetic.main.fragment_generated.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class GeneratedQrFragment : Fragment() {

    private val qrGenerator = QrGenerator()
    private val qrData = QrData()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_generated, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPref = context?.getSharedPreferences(Payment.PaymentPrefsKey, Context.MODE_PRIVATE)!!
        val data = qrData.qrDataWithApp(sharedPref)
        val bitmap = qrGenerator.generateQRCode(data)
        qr_code.setImageBitmap(bitmap)

        view.findViewById<ImageButton>(R.id.back_button).setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }

        view.findViewById<ImageButton>(R.id.noapp_button).apply {
            setOnClickListener { generateQrCodeForApp() }
            visibility = if (qrData.hasPaypal(sharedPref)) View.VISIBLE else View.GONE
        }
    }

    private fun generateQrCodeForApp() {
        val sharedPref = context?.getSharedPreferences(Payment.PaymentPrefsKey, Context.MODE_PRIVATE)!!
        val data = qrData.qrDataWithoutApp(sharedPref)
        val bitmap = qrGenerator.generateQRCode(data)
        qr_code.setImageBitmap(bitmap)
        showToast()
    }

    private fun showToast() {
        val text = "Wygenerowano kod QR dla użytkowników nieposiadających aplikacji"
        val centeredText = SpannableString(text)
        centeredText.setSpan(
            AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
            0, text.length - 1,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        val toast = Toast.makeText(context, centeredText, Toast.LENGTH_SHORT)
        toast.show()
    }
}