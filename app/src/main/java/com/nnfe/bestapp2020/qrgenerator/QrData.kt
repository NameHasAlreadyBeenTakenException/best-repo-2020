package com.nnfe.bestapp2020.qrgenerator

import android.content.SharedPreferences
import com.google.gson.Gson
import com.nnfe.bestapp2020.model.Payment

class QrData {

    private val gson = Gson()

    fun parseQrData(json: String): PaymentData {
        return gson.fromJson<PaymentData>(json, PaymentData::class.java)
    }

    fun qrDataWithApp(sharedPref: SharedPreferences): String {
        val paypalEmail = sharedPref.getString(Payment.PayPalEmailKey, null)
        val blikPhone = sharedPref.getString(Payment.BlikNumberKey, null)
        val data = PaymentData(
            paypalEmail = paypalEmail,
            blikPhone = blikPhone
        )
        return gson.toJson(data)
    }

    fun hasPaypal(sharedPref: SharedPreferences): Boolean {
        return sharedPref.getString(Payment.PayPalEmailKey, null) != null
    }

    fun hasAnyPaymentMethod(sharedPref: SharedPreferences): Boolean {
        val paypalEmail = sharedPref.getString(Payment.PayPalEmailKey, null)
        val blikPhone = sharedPref.getString(Payment.BlikNumberKey, null)
        return sharedPref.all.isNotEmpty() && (paypalEmail != null || blikPhone != null)
    }

    fun qrDataWithoutApp(sharedPref: SharedPreferences): String {
        val paypalEmail = sharedPref.getString(Payment.PayPalEmailKey, null)
        return "https://www.sandbox.paypal.com/cgi-bin/webscr?&cmd=_xclick&business=$paypalEmail&currency_code=PLN"
    }
}