package com.nnfe.bestapp2020.qrgenerator

data class PaymentData(
    val paypalEmail: String?,
    val blikPhone: String?
)