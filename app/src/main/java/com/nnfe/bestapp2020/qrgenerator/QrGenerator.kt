package com.nnfe.bestapp2020.qrgenerator

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter

class QrGenerator {

    private val codeWriter = MultiFormatWriter()

    fun generateQRCode(content: String): Bitmap {
        val bitmap = Bitmap.createBitmap(DefaultWidth, DefaultHeight, Bitmap.Config.ARGB_8888)

        val bitMatrix = codeWriter.encode(content, BarcodeFormat.QR_CODE, DefaultWidth, DefaultHeight)
        for (x in 0 until DefaultWidth) {
            for (y in 0 until DefaultHeight) {
                bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
            }
        }

        return bitmap
    }

    companion object {
        private const val DefaultWidth = 1000
        private const val DefaultHeight = 1000
    }
}