package com.nnfe.bestapp2020

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.paypal.android.sdk.payments.PayPalConfiguration
import com.paypal.android.sdk.payments.PayPalPayment
import com.paypal.android.sdk.payments.PayPalService
import com.paypal.android.sdk.payments.PaymentActivity
import java.math.BigDecimal


class PaymentDetailsActivity : AppCompatActivity() {
    private val currencies = arrayOf("PLN", "USD", "EUR", "GBP", "CHR", "JPY")
    private val clientId = "AX2OkLYWhAwuZAiQSRDPyblaqpqkAgseOVp4Wg_P08T-cXWkAC7wlluGn-aWcmpiNjYFgkdYul59ldtz"
    private val merchantName = "sb-hyo6u3657881@personal.example.com"
    private val BACK_TO_MAIN_ACTIVITY = 1

    var payeeEmail : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        payeeEmail = intent?.extras?.getString(PAYEE_EMAIL)

        setContentView(R.layout.activity_payment_details)
        val dropdownMenu = findViewById<Spinner>(R.id.currency_dropdown)
        val adapter: ArrayAdapter<String> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item, currencies
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dropdownMenu.adapter = adapter
    }



    fun onClickSubmitPayment(view: View) {

        if (!fieldsAreFilled()) {
            Toast.makeText(this, "Set amount", Toast.LENGTH_SHORT).show()
            return
        }
        val amountText = findViewById<EditText>(R.id.amount_edit_text)
        val amount = BigDecimal(amountText.text.toString())
        val currencyDropdown = findViewById<Spinner>(R.id.currency_dropdown)
        val currency = currencyDropdown.selectedItem.toString()
        val messageTextBox = findViewById<EditText>(R.id.message_text_box)
        val message = messageTextBox.text.toString()

        val intent = Intent(this, PayPalService::class.java)
        val config = PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(clientId)
            .merchantName(merchantName)

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        this.startService(intent)


        val paymentIntent = Intent(this, PaymentActivity::class.java)
        val payPalPayment = PayPalPayment(
            amount,
            currency,
            if (message.isBlank()) "Płatność" else message,
            PayPalPayment.PAYMENT_INTENT_SALE
        )
        payPalPayment.payeeEmail(payeeEmail)
        paymentIntent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)

        startActivityForResult(paymentIntent, BACK_TO_MAIN_ACTIVITY)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            BACK_TO_MAIN_ACTIVITY -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }


    private fun fieldsAreFilled(): Boolean {
        val amountText = findViewById<EditText>(R.id.amount_edit_text)
        if (amountText.text.isNotEmpty()) {
            return true
        }

        return false
    }

    companion object {
        val PAYEE_EMAIL = "PAYEE_EMAIL"
    }
}