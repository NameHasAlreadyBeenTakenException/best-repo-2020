package com.nnfe.bestapp2020

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button

import kotlinx.android.synthetic.main.payments_dialog.*

class AddPaymentsDialog(context: Context, val fragment: ManagePaymentsFragment) : Dialog(context)
{

    init {
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.payments_dialog)


        PayPal_button.setOnClickListener {
            fragment.OpenPayPalFragment("")
            dismiss()
        }

        Blik_button.setOnClickListener {
            fragment.OpenBlikFragment("")
            dismiss()
        }

        cancel_button.setOnClickListener {
            dismiss()
        }
    }
}