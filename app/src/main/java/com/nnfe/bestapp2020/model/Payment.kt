package com.nnfe.bestapp2020.model

import android.content.SharedPreferences

open class Payment {



    open fun getLabel(sharedPrefs: SharedPreferences) : String
    {
        return ""
    }

    open fun getDataName(sharedPrefs: SharedPreferences) : String
    {
        return ""
    }

    open fun getDataValue(sharedPrefs: SharedPreferences) : String
    {
        return ""
    }

    companion object {
        public const val PaymentPrefsKey: String = "payment_prefs_key"
        public const val BlikNumberKey: String = "blik_number_key"
        public const val PayPalEmailKey: String = "paypal_email_key"

        fun getPaymentMethodsData(sharedPrefs: SharedPreferences): ArrayList<Payment> {
            val payments = ArrayList<Payment>()
            if(sharedPrefs.contains(BlikNumberKey))
            {
                val blikNumber = sharedPrefs.getString(BlikNumberKey, null)
                if(blikNumber != null)
                {
                    payments.add(BlikPayment(blikNumber))
                }
            }
            if(sharedPrefs.contains(PayPalEmailKey))
            {
                val paypalEmail = sharedPrefs.getString(PayPalEmailKey, null)
                if(paypalEmail != null)
                {
                    payments.add(PayPalPayment(paypalEmail))
                }
            }
            return payments
        }
    }
}