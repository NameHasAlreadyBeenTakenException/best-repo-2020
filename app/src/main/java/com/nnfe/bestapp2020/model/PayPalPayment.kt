package com.nnfe.bestapp2020.model

import android.content.SharedPreferences

class PayPalPayment(val email: String) : Payment() {
    override fun getLabel(sharedPrefs: SharedPreferences): String {
        return "PayPal"
    }

    override fun getDataName(sharedPrefs: SharedPreferences): String {
        return "Adres email:"
    }

    override fun getDataValue(sharedPrefs: SharedPreferences): String {
        return sharedPrefs.getString(Payment.PayPalEmailKey, null)!!
    }
}