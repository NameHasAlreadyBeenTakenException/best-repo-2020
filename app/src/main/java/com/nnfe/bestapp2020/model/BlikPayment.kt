package com.nnfe.bestapp2020.model

import android.content.SharedPreferences

class BlikPayment(val phoneNumber: String) : Payment() {
    override fun getLabel(sharedPrefs: SharedPreferences): String {
        return "Blik"
    }

    override fun getDataName(sharedPrefs: SharedPreferences): String {
        return "Numer telefonu:"
    }

    override fun getDataValue(sharedPrefs: SharedPreferences): String {
        return sharedPrefs.getString(Payment.BlikNumberKey, null)!!
    }
}