package com.nnfe.bestapp2020.payment

import com.paypal.http.HttpResponse
import com.paypal.payouts.*

class PaymentManager {

    fun sample() {
        pay("Test_sdk_1234567", "1.00", "USD")
    }

    fun pay(batchId: String, amount: String, currency: String) {

        val items = listOf(
            PayoutItem()
                .senderItemId("Test_txn_test")
                .note("Your 5$ Payout!")
                .receiver("payout-sdk-test@paypal.com")
                .amount(
                    Currency()
                        .currency(amount)
                        .value(currency)
                )
        )

        val request = CreatePayoutRequest()
            .senderBatchHeader(
                SenderBatchHeader()
                    .senderBatchId(batchId)
                    .emailMessage("SDK payouts test txn")
                    .emailSubject("This is a test transaction from SDK")
                    .note("Enjoy your Payout!!")
                    .recipientType("EMAIL")
            )
            .items(items)


        val response: HttpResponse<CreatePayoutResponse> = PayPalConfig.client.execute(PayoutsPostRequest().requestBody(request))

        val payouts: CreatePayoutResponse = response.result()
        println("Payouts Batch ID: " + payouts.batchHeader().payoutBatchId())
        payouts.links().forEach {
            println(it.rel() + " => " + it.method() + ":" + it.href())
        }
    }
}