package com.nnfe.bestapp2020

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.media.Image
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.nnfe.bestapp2020.model.Payment
import com.nnfe.bestapp2020.qrgenerator.QrData

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ActionsFragment : Fragment() {

    private val qrData = QrData()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_actions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<ImageButton>(R.id.button_generate).setOnClickListener {
            val sharedPref = context?.getSharedPreferences(Payment.PaymentPrefsKey, Context.MODE_PRIVATE)!!
            if(qrData.hasAnyPaymentMethod(sharedPref)) {
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            } else {
                Toast.makeText(activity?.baseContext, "Nie dodano żadnej metody płatności!", Toast.LENGTH_SHORT).show()
            }
        }

        view.findViewById<ImageButton>(R.id.button_scan).setOnClickListener {


            if(ContextCompat.checkSelfPermission(requireActivity(),
                    Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    1);
            }
            else
            {
                (requireActivity() as MainActivity).startScanActivity()
            }
        }

        view.findViewById<ImageButton>(R.id.button_manage).setOnClickListener {
            findNavController().navigate(R.id.action_Actions_to_ManagePayments)
        }


    }
}