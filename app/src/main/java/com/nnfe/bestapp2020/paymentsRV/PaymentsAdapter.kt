package com.nnfe.bestapp2020.paymentsRV

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nnfe.bestapp2020.ManagePaymentsFragment
import com.nnfe.bestapp2020.R
import com.nnfe.bestapp2020.model.BlikPayment
import com.nnfe.bestapp2020.model.PayPalPayment
import com.nnfe.bestapp2020.model.Payment

class PaymentsAdapter(private val mPayments: List<Payment>, val fragment: ManagePaymentsFragment) : RecyclerView.Adapter<PaymentsAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val nameTextView = itemView.findViewById<TextView>(R.id.payment_type)
        val dataNameTextView = itemView.findViewById<TextView>(R.id.data_name)
        val dataValTextView = itemView.findViewById<TextView>(R.id.data_value)
        val editButton = itemView.findViewById<ImageButton>(R.id.edit_button)
        val deleteButton = itemView.findViewById<ImageButton>(R.id.delete_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentsAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.item_payment, parent, false)
        // Return a new holder instance
        return ViewHolder(contactView)
    }

    override fun onBindViewHolder(viewHolder: PaymentsAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val payment: Payment = mPayments.get(position)
        // Set item views based on your views and data model
        val textView = viewHolder.nameTextView
        val dataNameView = viewHolder.dataNameTextView
        val dataValView = viewHolder.dataValTextView
        val sharedPrefs = fragment.context?.getSharedPreferences(Payment.PaymentPrefsKey, Context.MODE_PRIVATE)
        textView.text = payment.getLabel(sharedPrefs!!)
        dataNameView.text = payment.getDataName(sharedPrefs)
        dataValView.text = payment.getDataValue(sharedPrefs)

        if(payment is BlikPayment)
        {

            viewHolder.editButton.setOnClickListener {
                fragment.OpenBlikFragment(sharedPrefs?.getString(Payment.BlikNumberKey, "")!!)
            }
            viewHolder.deleteButton.setOnClickListener {
                sharedPrefs?.edit()?.remove(Payment.BlikNumberKey)?.commit()
                fragment.ItemRemoved(position)
            }

        }
        else if(payment is PayPalPayment)
        {
            viewHolder.editButton.setOnClickListener {
                fragment.OpenPayPalFragment(sharedPrefs?.getString(Payment.PayPalEmailKey, "")!!)
            }
            viewHolder.deleteButton.setOnClickListener {
                sharedPrefs?.edit()?.remove(Payment.PayPalEmailKey)?.commit()
                fragment.ItemRemoved(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return mPayments.size
    }
}