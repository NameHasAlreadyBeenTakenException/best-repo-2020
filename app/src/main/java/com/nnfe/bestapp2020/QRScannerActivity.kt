package com.nnfe.bestapp2020

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.nnfe.bestapp2020.qrscanner.*

class QRScannerActivity : AppCompatActivity() {
    private var cc : CameraStateCallback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_q_r_scanner)

        val previewSurfaceView = findViewById<SurfaceView>(R.id.preview)
        previewSurfaceView.viewTreeObserver.addOnGlobalLayoutListener{
            if(PreviewContainer.Size == null)
            {
                val surfView = findViewById<SurfaceView>(R.id.preview)
                PreviewContainer.Size = Size(surfView.width, surfView.height)
            }
        }

        previewSurfaceView.holder.addCallback(object: SurfaceHolder.Callback{
            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
            }

            override fun surfaceCreated(holder: SurfaceHolder) {
                holder.setKeepScreenOn(true)

                val cm = getSystemService(Context.CAMERA_SERVICE) as CameraManager
                val id = cm!!.cameraIdList[0]
                val cameraCharacteristics = cm!!.getCameraCharacteristics(id)

                val previewSize = getPreviewOutputSize(previewSurfaceView.display, cameraCharacteristics, SurfaceHolder::class.java)
                previewSurfaceView.holder.setFixedSize(previewSize.width, previewSize.height)
                (previewSurfaceView as ScalableSurfaceView).setAspectRatio(previewSize)

                PreviewContainer.Surface = findViewById(R.id.preview)
                val top : LinearLayout = findViewById(R.id.top_container)

                val configurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.CAMERA)
                cc = CameraStateCallback(configurationMap!!, previewSurfaceView.holder.surface, this@QRScannerActivity)
                cm!!.openCamera(id, cc!!, null)
            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun closeCamera(){
        cc!!.imageReader!!.close()
        cc!!.camera!!.close()
    }
}